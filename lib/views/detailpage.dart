// ignore_for_file: unnecessary_new

import 'package:flutter/material.dart';
import 'package:unidad_3/controllers/databasehelpers.dart';
import 'package:unidad_3/views/editpage.dart';
import 'package:unidad_3/views/listviewuser.dart';

class Detail extends StatefulWidget {
  List list;
  int index;
  Detail({this.index, this.list});
  @override
  State<Detail> createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  DataBaseHelper databaseHelper = DataBaseHelper();

  _navigateList(BuildContext context) async {
    final result = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => ListUsers()));

    if (result) {
      setState(() {});
    }
  }

  //create function delete
  void confirm() {
    AlertDialog alertDialog = AlertDialog(
      content:
          Text("Esta seguto de eliminar '${widget.list[widget.index]['id']}'"),
      actions: <Widget>[
        RaisedButton(
          child: const Text(
            "OK remove!",
            style: TextStyle(color: Colors.black),
          ),
          color: Colors.red,
          onPressed: () {
            databaseHelper
                .removeRegister(widget.list[widget.index]['id'].toString());
            _navigateList(context);
          },
        ),
        RaisedButton(
          child: const Text("CANCEL", style: TextStyle(color: Colors.black)),
          color: Colors.green,
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );

    showDialog(context: context, builder: (_) => alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: 270.0,
          padding: const EdgeInsets.all(20.0),
          child: Card(
            child: Center(
              child: Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.only(top: 30.0),
                  ),
                  new Text(
                    widget.list[widget.index]['name'],
                    style: const TextStyle(fontSize: 20.0),
                  ),
                  const Divider(),
                  const Padding(
                    padding: EdgeInsets.only(top: 30.0),
                  ),
                  new Text(
                    widget.list[widget.index]['email'],
                    style: const TextStyle(fontSize: 20.0),
                  ),
                  const Divider(),
                  const Padding(
                    padding: EdgeInsets.only(top: 30.0),
                  ),
                  new Text(
                    widget.list[widget.index]['address'],
                    style: const TextStyle(fontSize: 20.0),
                  ),
                  const Divider(),
                  Row(
                    children: [
                      RaisedButton(
                        child: new Text("Edit"),
                        color: Colors.blueAccent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        onPressed: () =>
                            Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context) => new EditUserPage(
                            list: widget.list,
                            index: widget.index,
                          ),
                        )),
                      ),
                      VerticalDivider(),
                      RaisedButton(
                        child: new Text("Delete"),
                        color: Colors.redAccent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        onPressed: () => confirm(),
                      ),
                    ],
                  )
                ],
              ),
            ),
          )),
    );
  }
}
