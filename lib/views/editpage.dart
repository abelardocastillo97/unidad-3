import 'package:flutter/material.dart';
import 'package:unidad_3/views/listviewuser.dart';

import '../controllers/databasehelpers.dart';

class EditUserPage extends StatefulWidget {
  final List list;
  final int index;

  const EditUserPage({this.list, this.index});

  @override
  State<EditUserPage> createState() => _EditUserPageState();
}

class _EditUserPageState extends State<EditUserPage> {
  DataBaseHelper dataBaseHelper = DataBaseHelper();

  TextEditingController controllerName;
  TextEditingController controllerEmail;
  TextEditingController controllerAddress;
  TextEditingController controllerId;

  _navigateList(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ListUsers()),
    );

    if (result) {
      setState(() {});
    }
  }

  @override
  void initState() {
    controllerId = new TextEditingController(
        text: widget.list[widget.index]['id'].toString());
    controllerName = new TextEditingController(
        text: widget.list[widget.index]['name'].toString());
    controllerEmail = new TextEditingController(
        text: widget.list[widget.index]['email'].toString());
    controllerAddress = new TextEditingController(
        text: widget.list[widget.index]['address'].toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        child: ListView(
          padding: const EdgeInsets.all(10.0),
          children: [
            Column(
              children: [
                ListTile(
                  leading: const Icon(
                    Icons.bookmark,
                    color: Colors.black,
                  ),
                  title: new TextFormField(
                    controller: controllerId,
                    decoration: new InputDecoration(
                      hintText: "Id",
                      labelText: "Id",
                    ),
                  ),
                ),
                ListTile(
                  leading: const Icon(
                    Icons.person,
                    color: Colors.black,
                  ),
                  title: new TextFormField(
                    controller: controllerName,
                    decoration: new InputDecoration(
                      hintText: "name",
                      labelText: "name",
                    ),
                  ),
                ),
                ListTile(
                  leading: const Icon(
                    Icons.email,
                    color: Colors.black,
                  ),
                  title: new TextFormField(
                    controller: controllerEmail,
                    decoration: new InputDecoration(
                      hintText: "Email",
                      labelText: "Email",
                    ),
                  ),
                ),
                ListTile(
                  leading: const Icon(
                    Icons.home,
                    color: Colors.black,
                  ),
                  title: new TextFormField(
                    controller: controllerAddress,
                    decoration: new InputDecoration(
                      hintText: "Address",
                      labelText: "Address",
                    ),
                  ),
                ),
                const Divider(
                  height: 1.0,
                ),
                new Padding(
                  padding: const EdgeInsets.all(10.0),
                ),
                RaisedButton(
                  child: new Text("Edit"),
                  color: Colors.blueAccent,
                  onPressed: () {
                    dataBaseHelper.editUser(
                        controllerId.text.trim(),
                        controllerName.text.trim(),
                        controllerAddress.text.trim(),
                        controllerAddress.text.trim());
                    _navigateList(context);
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
