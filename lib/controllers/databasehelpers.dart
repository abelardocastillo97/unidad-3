import 'package:http/http.dart' as http;
import 'dart:convert';

class DataBaseHelper {
  //AddUser
  Future<http.Response> addUser(String nameController, String emailController,
      String addressController) async {
    var url = 'http://192.168.56.1:8787/addUser';
    Map data = {
      'name': '$nameController',
      'email': '$emailController',
      'address': '$addressController'
    };

    var body = json.encode(data);

    var response = await http.post(Uri.parse(url),
        headers: {"Content-Type": "application/json"}, body: body);
    print("${response.statusCode}");
    print("${response.body}");
    return response;
  }

  //UpdateUser
  Future<http.Response> editUser(String id, String nameController,
      String emailController, String addressController) async {
    int a = int.parse(id);
    print(a);
    var url = 'http://192.168.56.1:8787/update';

    Map data = {
      'id': '$a',
      'name': '$nameController',
      'email': '$emailController',
      'address': '$addressController'
    };

    var body = json.encode(data);

    var response = await http.put(Uri.parse(url),
        headers: {"Content-Type": "application/json"}, body: body);
    print("${response.statusCode}");
    print("${response.body}");
    return response;
  }

  //DeleteUser
  Future<http.Response> removeRegister(String id) async {
    int a = int.parse(id);
    print(a);
    var url = 'http://192.168.56.1:8787/delete/$a';

    var response = await http
        .delete(Uri.parse(url), headers: {"Content-Type": "application/json"});
    print("${response.statusCode}");
    return response;
  }
}
